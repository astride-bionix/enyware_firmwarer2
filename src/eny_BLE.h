#ifndef _BLE_H_
#define _BLE_H_


#include <main.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

#include "esp_ota_ops.h"

#define DeviceName      "Enyware"
#define TXbufferSize    512

extern bool deviceConnected;
extern bool OTAstart;

class BLE; // forward declaration

class BLECustomServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      // // code here for when device connects
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      // // code here for when device disconnects
    }
};

class MyCallbacks : public BLECharacteristicCallbacks
{

public:
    MyCallbacks(BLE* mble) {
      _m_ble = mble;
    }
    BLE* _m_ble;

void onWrite(BLECharacteristic *pCharacteristic);

};

// class MyServerCallbacks : public BLEServerCallbacks {
//     void onConnect(BLEServer* pServer) {
//     //   _BLEClientConnected = true;
//       deviceConnected = true;
//     };

//     void onDisconnect(BLEServer* pServer) {
//     //   _BLEClientConnected = false;
//       deviceConnected = false;
//     }
// };

class otaCallback: public BLECharacteristicCallbacks {
  public:
    otaCallback(BLE* ble) {
      _p_ble = ble;
    }
    BLE* _p_ble;

    void onWrite(BLECharacteristic *pCharacteristic);
};



class BLE
{

  uint8_t batt_lv, chg_stat;
  public:

    BLE(void);
    ~BLE(void);

    bool begin(const char* localName);
    void updateBattery(uint8_t level);
    void updateBattCharging(uint8_t status);
    bool run_enywareBLE_connected();
    void Send_Data_BLE(String dataBuffer);
    void Send_Databuffer_BLE(uint8_t *buff, int len);
    // void command_rec(uint8_t *buff, int len);
    void updateBatt_chg_to_sensorUUID();
  
  private:
    String local_name;

    BLEServer *pServer = NULL;

    BLEService *pESPOTAService = NULL;
    BLECharacteristic * pESPOTAIdCharacteristic = NULL;

    BLEService *pService = NULL;
    BLECharacteristic * pVersionCharacteristic = NULL;
    BLECharacteristic * pOtaCharacteristic = NULL;

    BLEService *pBattery    = NULL;

    BLEService *pEnyTable = NULL;
    BLECharacteristic * pTxCharacteristic = NULL;
    BLECharacteristic *pRxCharacteristic = NULL;
    BLECharacteristic * pBattCharacteristic = NULL;

    // BLEService *pNewBatt = NULL;
    // BLECharacteristic * pBattCharacteristic = NULL;
    // BLECharacteristic *pChargeCharacteristic = NULL;

    BLEAdvertising *pAdvertising = NULL;
};

#endif







// void init_enywareBLE();
// bool run_enywareBLE_connected();
// void Send_Data_BLE(String dataBuffer);
// void Send_Databuffer_BLE(uint8_t *buff, int len);
// void SetConfig_Name_BLE();

