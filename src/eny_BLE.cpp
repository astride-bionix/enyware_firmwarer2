#include <eny_BLE.h>
#include <utility.h>

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

// https://gist.github.com/sam016/4abe921b5a9ee27f67b3686910293026

#define SERVICE_UUID_ESPOTA "d804b643-6ce7-4e81-9f8a-ce0f699085eb"
#define CHARACTERISTIC_UUID_ID "d804b644-6ce7-4e81-9f8a-ce0f699085eb"

#define SERVICE_UUID_OTA "c8659210-af91-4ad3-a995-a58d6fd26145"
#define CHARACTERISTIC_UUID_FW "c8659211-af91-4ad3-a995-a58d6fd26145"
#define CHARACTERISTIC_UUID_HW_VERSION "c8659212-af91-4ad3-a995-a58d6fd26145"

// //https://github.com/ARMmbed/ble/blob/master/source/services/DFUService.cpp
// #define SERVICE_UUID_OTA                    "00001530-1212-efde-1523-785feabcd123"
// #define CHARACTERISTIC_UUID_FW              "00001531-1212-efde-1523-785feabcd123"
// #define CHARACTERISTIC_UUID_HW_VERSION      "00001533-1212-efde-1523-785feabcd123"

// #define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
// #define SERVICE_UUID "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID

#define SERVICE_UUID_SENSOR "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_BATT "82b6dd65-b68f-11ec-b909-0242ac120002"


#define FULL_PACKET 125
#define CHARPOS_UPDATE_FLAG 5

// BLE profile see : https://www.bluetooth.com/specifications/assigned-numbers/health-device-profile/

#define BatteryService BLEUUID((uint16_t)0x180F)
#define MotionService BLEUUID((uint16_t)0x107B)

BLECharacteristic BatteryLevelCharacteristic(BLEUUID((uint16_t)0x2A19), BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
BLECharacteristic BatteryChargeCharacteristic(BLEUUID((uint16_t)0x2A1A), BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);

// #define BatteryService BLEUUID((uint16_t)0x1A1A)
// #define MotionService BLEUUID((uint16_t)0x1B1B)

// BLECharacteristic BatteryLevelCharacteristic(BLEUUID((uint16_t)0x2C2C), BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
// BLECharacteristic BatteryChargeCharacteristic(BLEUUID((uint16_t)0x2C3D), BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);




// BLEDescriptor BatteryLevelDescriptor(BLEUUID((uint16_t)0x2901));

esp_ota_handle_t otaHandler = 0;

const uint8_t DFUServiceBaseUUID[] = {
    0x00,
    0x00,
    0x00,
    0x00,
    0x12,
    0x12,
    0xEF,
    0xDE,
    0x15,
    0x23,
    0x78,
    0x5F,
    0xEA,
    0xBC,
    0xD1,
    0x23,
};

// BLEServer *pServer = NULL;
// BLECharacteristic *pTxCharacteristic;

bool OTAstart = false;

bool deviceConnected = false;
bool oldDeviceConnected = false;
uint8_t txValue = 0;
bool Status_Connect = false;
bool isNullRxBT = false;
String RxBlutooth = "";

char bleDebugbuf[TXbufferSize];
char bleTXbuffer[TXbufferSize];

char blebuf[256];

uint8_t data_ex[517] = {};

bool updateFlag = false;
bool readyFlag = false;
int bytesReceived = 0;
int timesWritten = 0;
bool _BLEClientConnected = false;

//---------------------- OTA ------------------------------

void otaCallback::onWrite(BLECharacteristic *pCharacteristic)
{
  std::string rxData = pCharacteristic->getValue();
  if (!updateFlag)
  { // If it's the first packet of OTA since bootup, begin OTA
    Serial.println("[OTA] -> BeginOTA");
    OTAstart = true;
    esp_ota_begin(esp_ota_get_next_update_partition(NULL), OTA_SIZE_UNKNOWN, &otaHandler);
    updateFlag = true;
  }
  if (_p_ble != NULL)
  {
    if (rxData.length() > 0)
    {
      esp_ota_write(otaHandler, rxData.c_str(), rxData.length());
      if (rxData.length() != FULL_PACKET)
      {
        esp_ota_end(otaHandler);
        Serial.println("[OTA] -> EndOTA");
        if (ESP_OK == esp_ota_set_boot_partition(esp_ota_get_next_update_partition(NULL)))
        {
          delay(2000);
          esp_restart();
        }
        else
        {
          Serial.println("[OTA] -> Upload Error");
          OTAstart = false;
        }
      }
    }
  }

  uint8_t txData[5] = {1, 2, 3, 4, 5};
  // delay(1000);
  pCharacteristic->setValue((uint8_t *)txData, 5);
  pCharacteristic->notify();
}

//-------------------- BLE Normal -----------------------------
void MyCallbacks::onWrite(BLECharacteristic *pCharacteristic)
{
  std::string rxValue = pCharacteristic->getValue();
  if (rxValue.length() > 0)
  {
    char rx_ch[20];
    // Serial.println("*********");
    // Serial.print("Received Value: ");
    for (int i = 0; i < rxValue.length(); i++)
    {
      // Serial.print(rxValue[i], HEX);
      // Serial.print(" ");
      rx_ch[i] = rxValue[i];
    }

    command_rec((uint8_t *)rx_ch, rxValue.length());
  }
}

//
// Constructor
BLE::BLE(void)
{
}

//
// Destructor
BLE::~BLE(void)
{
}

//
// begin
bool BLE::begin(const char *localName = "UART Service")
{
  // Create the BLE Device
  BLEDevice::init(localName);

  BLEDevice::setPower(ESP_PWR_LVL_N14);

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new BLECustomServerCallbacks());

  // Create the BLE Service
  pBattery = pServer->createService(BatteryService);
  pEnyTable = pServer->createService(SERVICE_UUID_SENSOR);

  pESPOTAService = pServer->createService(SERVICE_UUID_ESPOTA);
  pService = pServer->createService(SERVICE_UUID_OTA);

  // pNewBatt = pServer->createService(SERVICE_UUID_BATT);

  // Create a BLE Characteristic
  // --- Battery level ---
  pBattery->addCharacteristic(&BatteryLevelCharacteristic);
  // BatteryLevelDescriptor.setValue("Percentage 0 - 100");
  // BatteryLevelCharacteristic.addDescriptor(&BatteryLevelDescriptor);
  pBattery->addCharacteristic(&BatteryChargeCharacteristic);
  BatteryLevelCharacteristic.addDescriptor(new BLE2902());
  BatteryChargeCharacteristic.addDescriptor(new BLE2902());

  // --- BLE UART  ---
  pTxCharacteristic = pEnyTable->createCharacteristic(CHARACTERISTIC_UUID_TX, BLECharacteristic::PROPERTY_NOTIFY);
  pTxCharacteristic->addDescriptor(new BLE2902());

  pRxCharacteristic = pEnyTable->createCharacteristic(CHARACTERISTIC_UUID_RX, BLECharacteristic::PROPERTY_WRITE);
  pRxCharacteristic->setCallbacks(new MyCallbacks(this));

  pBattCharacteristic = pEnyTable->createCharacteristic(CHARACTERISTIC_UUID_BATT, BLECharacteristic::PROPERTY_NOTIFY);
  pBattCharacteristic->addDescriptor(new BLE2902());

  // --- OTA Update ---
  pESPOTAIdCharacteristic = pESPOTAService->createCharacteristic(
      CHARACTERISTIC_UUID_ID,
      BLECharacteristic::PROPERTY_READ);

  pVersionCharacteristic = pService->createCharacteristic(
      CHARACTERISTIC_UUID_HW_VERSION,
      BLECharacteristic::PROPERTY_READ);

  pOtaCharacteristic = pService->createCharacteristic(
      CHARACTERISTIC_UUID_FW,
      BLECharacteristic::PROPERTY_NOTIFY | BLECharacteristic::PROPERTY_WRITE);

  pOtaCharacteristic->addDescriptor(new BLE2902());
  pOtaCharacteristic->setCallbacks(new otaCallback(this));

  // // --- New Batt service ---
  // pBattCharacteristic = pNewBatt->createCharacteristic(CHARACTERISTIC_UUID_BATT, BLECharacteristic::PROPERTY_NOTIFY);
  // pBattCharacteristic->addDescriptor(new BLE2902());

  // Start the service(s)
  pBattery->start();
  pEnyTable->start();
  pESPOTAService->start();
  pService->start();
  // pNewBatt->start();

  pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID_SENSOR);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06); // functions that help with iPhone connections issue
  // pAdvertising->setMinPreferred(0x12);

  BLEDevice::startAdvertising();

  // Start advertising

  pServer->getAdvertising()->addServiceUUID(BatteryService);
  pServer->getAdvertising()->addServiceUUID(SERVICE_UUID_ESPOTA);
  // pServer->getAdvertising()->addServiceUUID(SERVICE_UUID_BATT);
  pServer->getAdvertising()->start();

  uint8_t VERSION_PATCH = (uint8_t)atoi(BUILD_NUMBER);
  uint8_t hardwareVersion[5] = {HARDWARE_VERSION_MAJOR, HARDWARE_VERSION_MINOR, SOFTWARE_VERSION_MAJOR, SOFTWARE_VERSION_MINOR, VERSION_PATCH};
  // uint8_t hardwareVersion[5] = {HARDWARE_VERSION_MAJOR, HARDWARE_VERSION_MINOR, SOFTWARE_VERSION_MAJOR, SOFTWARE_VERSION_MINOR, SOFTWARE_VERSION_PATCH};
  pVersionCharacteristic->setValue((uint8_t *)hardwareVersion, 5);

  return true;
}

void BLE::updateBattery(uint8_t level)
{
  batt_lv = level;
  BatteryLevelCharacteristic.setValue(&level, 1);
  BatteryLevelCharacteristic.notify();
}

void BLE::updateBattCharging(uint8_t status)
{

  uint8_t data_chg;

  if (status == 1)
    data_chg = 0x65;
  else if (status == 0)
    data_chg = 0x75;

  chg_stat = data_chg;
  BatteryChargeCharacteristic.setValue(&data_chg, 1);
  BatteryChargeCharacteristic.notify();
}

void BLE::updateBatt_chg_to_sensorUUID(){
  uint8_t batt_chg[2];
  batt_chg[0] = batt_lv;
  batt_chg[1] = chg_stat;

  pBattCharacteristic->setValue(batt_chg,2);
  pBattCharacteristic->notify();
}

bool BLE::run_enywareBLE_connected()
{
  // disconnecting
  if (!deviceConnected && oldDeviceConnected)
  {
    pServer->startAdvertising(); // restart advertising
    Serial.println("\n-> start advertising\n");
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  else if (deviceConnected && !oldDeviceConnected)
  {
    Serial.println("\n-> Device connected!\n");
    oldDeviceConnected = deviceConnected;
    Status_Connect = false;
  }

  if (deviceConnected)
  {
    Status_Connect = true;
  }
  else
  {
    Status_Connect = false;
  }

  if (isNullRxBT == true)
  {
    String Strrec = String("Recieve RxBlutooth : " + RxBlutooth);
    Serial.println(Strrec);
    isNullRxBT = false;
  }
  return Status_Connect;
}

void BLE::Send_Data_BLE(String dataBuffer)
{
  pTxCharacteristic->setValue(dataBuffer.c_str());
  pTxCharacteristic->notify();

  // dataBuffer.toCharArray(bleDebugbuf,dataBuffer.length()+1);
  // Serial.printf(bleDebugbuf,"<--[TX]{size:%d byte} : ", dataBuffer.length());
  //  Serial.print(bleDebugbuf);  Serial.println(dataBuffer);
}

void BLE::Send_Databuffer_BLE(uint8_t *buff, int len)
{
  pTxCharacteristic->setValue(buff, len);
  pTxCharacteristic->notify();

  // dataBuffer.toCharArray(bleDebugbuf,dataBuffer.length()+1);
  // Serial.printf(bleDebugbuf,"<--[TX]{size:%d byte} : ", dataBuffer.length());
  // Serial.print(bleDebugbuf);  Serial.println(dataBuffer);
}

// // class MyServerCallbacks : public BLEServerCallbacks
// // {
// //   void onConnect(BLEServer *pServer)
// //   {
// //     deviceConnected = true;
// //   };

// //   void onDisconnect(BLEServer *pServer)
// //   {
// //     deviceConnected = false;
// //   }
// // };

// // class MyCallbacks : public BLECharacteristicCallbacks
// // {
// //   void onWrite(BLECharacteristic *pCharacteristic)
// //   {
// //     std::string rxValue = pCharacteristic->getValue();

// //     if (rxValue.length() > 0)
// //     {
// //       Serial.println("*********");
// //       Serial.print("Received Value: ");
// //       for (int i = 0; i < rxValue.length(); i++)
// //         Serial.print(rxValue[i]);

// //       Serial.println();
// //       Serial.println("*********");
// //     }
// //   }
// // };

// void init_enywareBLE()
// {

//   // Create the BLE Device

//   // BLEDevice::init(DeviceName);
//   // BLEDevice::setMTU(512);
//   // // Create the BLE Server
//   // pServer = BLEDevice::createServer();
//   // pServer->setCallbacks(new MyServerCallbacks());

//   // Create the BLE Service
//   // BLEService *pService = pServer->createService(SERVICE_UUID);

//   // //Create a BLE Characteristic
//   // pTxCharacteristic = pService->createCharacteristic(
//   //     CHARACTERISTIC_UUID_TX,
//   //     BLECharacteristic::PROPERTY_NOTIFY);

//   // pTxCharacteristic->addDescriptor(new BLE2902());

//   // BLECharacteristic *pRxCharacteristic = pService->createCharacteristic(
//   //     CHARACTERISTIC_UUID_RX,
//   //     BLECharacteristic::PROPERTY_WRITE);

//   // pRxCharacteristic->setCallbacks(new MyCallbacks());

//   // Start the service
//   // pService->start();
//   // BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
//   // pAdvertising->addServiceUUID(SERVICE_UUID);
//   // pAdvertising->setScanResponse(true);
//   // pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
//   // pAdvertising->setMinPreferred(0x12);
//   // BLEDevice::startAdvertising();

//   // // Start advertising
//   // pServer->getAdvertising()->start();
//   // Serial.println("Waiting a client connection to notify...");
// }

// void SetConfig_Name_BLE(){
// //  char NameBT[] = "BLE-";
// //   char BufNameBT [50];
// //   char Name_Str[50];
// //   int Int_FlagTPMS = 0;
// //   String Set_buf ;
// //   if ((EEPROM.read(114) == 0xFF ) || (EEPROM.read(114) == 0x00 )) { //  buffer_test[0]
// //     Flag_Config_Serial = false ;//true;
// //     BLEDevice::init("BLE-NOTCONFIG");
// //     Serial.println("--------------------------------------------");
// //     Serial.println("[BLE: NOT CONFIG NAME !!]");
// //     Serial.println("->Please Config Serial ID. . . ");
// //     Serial.println("--------------------------------------------");
// //   }
// //     else {
// //     for (int i = 0; i < 25; i++) { //25
// //       buffer_test[i] =  EEPROM.read(114 + i) ;
// //     }
// //     sprintf(BufNameBT, "%s%s", NameBT, buffer_test);
// //     std::string Serial_Name (BufNameBT);
// //     BLEDevice::init(Serial_Name);
// //     Flag_Config_Serial = true;
// //     Serial.println("--------------------------------------------");
// //     Serial.println("[BLE : CONFIG NAME OK !!]");
// //     Serial.print("[BLE Name: "); Serial.print(BufNameBT); Serial.println("]");
// //     Serial.println("---------------------------------------------");
//   }
