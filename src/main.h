#include <Arduino.h>
#include <version.h>
#include <Wire.h>

#define fwver       004
#define FWversion   VERSION
#define Build_DT    VERSION_DATETIME

#define addr_isData     0x00    //1byte
#define addr_battCnt    0x01    //4byte

#define MTU_reqBuffer   517

#define stx             0x02
#define eot             0x04

#define CMDtableTX      0x1
#define CMDseting       0xA4


#define SOFTWARE_VERSION_MAJOR 1
#define SOFTWARE_VERSION_MINOR 0
// #define SOFTWARE_VERSION_PATCH 1

#define HARDWARE_VERSION_MAJOR 1
#define HARDWARE_VERSION_MINOR 2

void Main_task(void *pvParameter);
void MPU_task(void *pvParameter);

void command_rec(uint8_t *buff, int len);

