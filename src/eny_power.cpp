#include <eny_power.h>
#include <ESP32AnalogRead.h>
#include <utility.h>
#include <eny_BLE.h>

ESP32AnalogRead adc_vin;
TickTask readBattTick(3000UL);
TickTask startTick(5000UL);
TickTask secCntTick(1000UL);
char hwbuf[128];

RTC_DATA_ATTR int bootCount = 0;


void eny_Hardware::mainPowerCrl(bool pwr)
{
    if (pwr == ON)
    {
        digitalWrite(PWR_MAIN, ON);
    }
    else
    {
        //Go to sleep now
        esp_deep_sleep_start();
        
        digitalWrite(PWR_MAIN, OFF);
    }
}

//Function that prints the reason by which ESP32 has been awaken from sleep
void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;
  wakeup_reason = esp_sleep_get_wakeup_cause();
  switch(wakeup_reason)
  {
    case 1  : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case 2  : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case 3  : Serial.println("Wakeup caused by timer"); break;
    case 4  : Serial.println("Wakeup caused by touchpad"); break;
    case 5  : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.println("Wakeup was not caused by deep sleep"); break;
  }
}

void eny_Hardware::init_hardware()
{
    pinMode(LED_RED, OUTPUT);
    pinMode(LED_GRN, OUTPUT);
    pinMode(LED_BLU, OUTPUT);

    pinMode(Button, INPUT);
    pinMode(PGOOD, INPUT);
    pinMode(CHG_STAT, INPUT);
    pinMode(PWR_MAIN, OUTPUT);

    //Increment boot number and print it every reboot
    ++bootCount;
    Serial.println("Boot number: " + String(bootCount));

    //Print the wakeup reason for ESP32
    print_wakeup_reason();

    //Configure GPIO33 as ext0 wake up source for HIGH logic level
    esp_sleep_enable_ext0_wakeup(GPIO_NUM_13,0);

    // //Go to sleep now
    // esp_deep_sleep_start();

    if (!EEPROM.begin(EEPROM_SIZE))
    {
        Serial.println("-> failed to initialise EEPROM");
    }

    ledRGB(0, 1, 0);
    adc_vin.attach(ADC_Batt);
    // adc1_config_width(ADC_WIDTH_12Bit);
    // adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_0db); //set reference voltage to internal
    mainPowerCrl(ON);

    while (1)
    {
        batt_per = Read_Batt_percent();
        preBatt += read_vBatt();
        preCntBatt++;

        if (readBattTick.Update())
        {
            batt_v = (preBatt / (float)preCntBatt);
            preCntBatt = 0;
            preBatt = 0;
        }

        if (startTick.Update())
        {
            break;
        }
    }
}

void eny_Hardware::ledRGB(bool r, bool g, bool b)
{
    digitalWrite(LED_RED, !r);
    digitalWrite(LED_GRN, !g);
    digitalWrite(LED_BLU, !b);
}

float filterConstant = 0.975; // filter constant

float eny_Hardware::smooth(float data, float filterVal, float smoothedVal)
{
    smoothedVal = (data * (1 - filterVal)) + (smoothedVal * filterVal);
    return smoothedVal;
}

float eny_Hardware::read_vBatt()
{
    float R1 = 22000.00;
    float R2 = 22000.00;

    float voltage = adc_vin.readVoltage();
    // float vbatt = (newVal / (R2 / (R1 + R2)));
    float vbatt = (voltage / (R2 / (R1 + R2)));
    if (vbatt > max_batt)
        vbatt = max_batt;
    return vbatt;
}

int eny_Hardware::Read_Batt_percent()
{
    int percent = (int)(((batt_v - min_batt) * 100.0f) / (max_batt - min_batt));
    if (percent >= 100)
        percent = 100;
    else if (percent <= 0)
        percent = 0;
    return percent;
}

bool eny_Hardware::get_ChgStatus()
{
    return digitalRead(CHG_STAT);
}

bool eny_Hardware::get_Pgood()
{
    return digitalRead(PGOOD);
}

bool eny_Hardware::readButton() { return !button; }

void eny_Hardware::pwrMNG()
{
    currentMill = millis();
    if (readButton() == true)
    {
        if (statT == false)
        {
            PushedMill = currentMill;
            statT = true;
        }
        Ready = true;
    }
    else
    {
        Ready = false;
        statT = false;
    }

    if (Ready == true)
    {

        if ((unsigned long)(currentMill - PushedMill) >= turnOnDelay)
        {
            if (get_Pgood() == 1)
            {
                Serial.println("-> MAIN POWER CONTROL [OFF] <-");
                Serial.println("******************************");
                Ready = false;
                statT = false;
                ledRGB(1, 1, 1);
                seve_onbatt_time(onBattTime);
                saveStat(0xCC);
                delay(1000);
                mainPowerCrl(OFF); // Shutdown system
                delay(1000);
            }
        }
        else
            saveStat(0xFF);
    }

    // if ((batt_v <= min_batt) && (get_Pgood() == 1))

     if ((Read_Batt_percent() <= min_battper) && (get_Pgood() == 1))   //<= 5% : shutdown
    {
        Serial.println("-> LOW BATTERY SYSTEM IS SHUTDOWN <-");
        Serial.println("******************************");
        Ready = false;
        statT = false;
        ledRGB(1, 1, 1);
        seve_onbatt_time(onBattTime);
        saveStat(0xCD);
        delay(1000);
        mainPowerCrl(OFF); // Shutdown system
        delay(1000);
    }
}

void eny_Hardware::enyHWrun()
{
    pwrMNG();

    if(OTAstart == false){
        button = digitalRead(Button);
    }else{
        button = 1;
    }


    batt_per = Read_Batt_percent();
    preBatt += read_vBatt();
    preCntBatt++;

    if (readBattTick.Update())
    {
        batt_v = (preBatt / (float)preCntBatt);
        preCntBatt = 0;
        preBatt = 0;
    }

    if (get_ChgStatus() == 0)
    {      
        // digitalWrite(LED_RED, 0);  
    }
    else
    {
        // digitalWrite(LED_RED, 1);
    }

    if (secCntTick.Update())
    {
        if (get_Pgood() == 1)
        {
            if (get_ChgStatus() == 1)
            {
                onBattTime++;
            }
        }
    }
}

void eny_Hardware::saveStat(uint stat)
{
    EEPROM.write(addr_isData, stat);
    EEPROM.commit();
}

void eny_Hardware::seve_onbatt_time(uint32_t val)
{
    uint8_t valByte[4];
    uint8_t addr = addr_battCnt;

    valByte[0] = (uint8_t)(uint8_t)(val >> 24) & 0xFF;
    valByte[1] = (uint8_t)(val >> 16) & 0xFF;
    valByte[2] = (uint8_t)(val >> 8) & 0xFF;
    valByte[3] = (uint8_t)(val & 0xFF);

    for (int i = 0; i < 4; i++)
    {
        EEPROM.write(addr++, valByte[i]);
        EEPROM.commit();
    }

    // sprintf(hwbuf,"TX data : 0x%x,0x%x,0x%x,0x%x", valByte[0], valByte[1], valByte[2], valByte[3]);
    // Serial.println(hwbuf);
}

uint32_t eny_Hardware::read_onbatt_time(void)
{
    uint8_t valByte[4];
    uint8_t addrRx = addr_battCnt;

    for (int i = 0; i < 4; i++)
    {
        valByte[i] = EEPROM.read(addrRx++);
    }

    // sprintf(hwbuf,"RX data : 0x%x,0x%x,0x%x,0x%x", valByte[0], valByte[1], valByte[2], valByte[3]);
    // Serial.println(hwbuf);

    return (uint32_t)((valByte[0] * 256 * 256 * 256) + (valByte[1] * 256 * 256) + (valByte[2] * 256) + valByte[3]);
}

float eny_Hardware::getBattVolt() { return batt_v; }
float eny_Hardware::getBattPerC() { return batt_per; }
uint32_t eny_Hardware::getOnBattTime() { return onBattTime; }

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_ENY_POWER)
eny_Hardware enyHW;
#endif