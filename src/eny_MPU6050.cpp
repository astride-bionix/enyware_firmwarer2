#include <eny_MPU6050.h>
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <KX0231025IMU.h>
#include <utility.h>

#define no_imu 0
#define mpu6050 1
#define KX0231025 2

#define KX0231025_RANGE_2G 0
#define KX0231025_RANGE_4G 1
#define KX0231025_RANGE_8G 2

#define KX0231025_DATARATE_12_5HZ 0
#define KX0231025_DATARATE_25HZ 1
#define KX0231025_DATARATE_50HZ 2
#define KX0231025_DATARATE_100HZ 3
#define KX0231025_DATARATE_200HZ 4
#define KX0231025_DATARATE_400HZ 5
#define KX0231025_DATARATE_800HZ 6
#define KX0231025_DATARATE_1600HZ 7
#define KX0231025_DATARATE_0_781HZ 8
#define KX0231025_DATARATE_1_563HZ 9
#define KX0231025_DATARATE_3_125HZ 10
#define KX0231025_DATARATE_6_25HZ 11

#define KX0231025_LOWPOWER_MODE 0X00
#define KX0231025_HIGHPOWER_MODE 0X40

// MPU6050 accelgyro(0x69); // <-- use for AD0 high
Adafruit_MPU6050 mpu;
Adafruit_Sensor *mpu_temp, *mpu_accel, *mpu_gyro;

KX0231025Class *imu;

char gbuf[256];
uint8_t MemsData[6];

bool init_ok;
uint8_t imu_select;

int16_t ax, ay, az;
// int16_t gx, gy, gz;

// float ax, ay, az;

const int MPU = 0x69; // MPU6050 I2C address

bool init_enyware_MPU6050()
{
  // Wire.begin();
  // Wire.setClock(50000);

  imu_select = KX0231025;

  imu = new KX0231025Class(Wire, 0x1E);

  if (imu->begin(KX0231025_LOWPOWER_MODE, KX0231025_RANGE_8G, KX0231025_DATARATE_100HZ))
  {
    Serial.println("-> init IMU : KX0231025 OK");
    init_ok = true;
  }
  else
  {
    init_ok = false;
  }

  // byte address, error;
  // uint8_t imu_addr;

  // for (address = 1; address < 127; address++)
  // {
  //   error = checkI2C(address);
  //   if (error == 0)
  //   {
  //     Serial.print("-> I2C device found at address 0x");
  //     Serial.print(address, HEX);
  //     Serial.println("  !");
  //     imu_addr = address;
  //   }
  //   else if (error == 4)
  //   {
  //     Serial.print("-> Unknown error at address 0x");
  //     if (address < 16)
  //     {
  //       Serial.print("0");
  //       Serial.println(address, HEX);
  //     }
  //   }
  // }

  // sprintf(gbuf, "-> IMU I2C Addr : 0x%2X", imu_addr);
  // Serial.println(gbuf);

  // uint8_t err = checkI2C(0x1E);
  // if (err == 0)
  // {
  //   if (imu_addr == 0x1E)
  //   {
  //     imu_select = KX0231025;

  //     imu = new KX0231025Class(Wire, 0x1E);

  //     if (imu->begin(KX0231025_LOWPOWER_MODE, KX0231025_RANGE_8G, KX0231025_DATARATE_100HZ))
  //     {
  //       Serial.println("KX0231025 Found!");
  //       init_ok = true;
  //     }
  //     else
  //     {
  //       init_ok = false;
  //     }
  //   }
  // }

  // if (imu_addr == 0x69)
  // {
  //   // Try to initialize!
  //   imu_select = mpu6050;
  //   if (!mpu.begin(0x69))
  //   {
  //     init_ok = false;
  //     Serial.println("Failed to find MPU6050 chip");
  //   }
  //   else
  //   {
  //     Serial.println("MPU6050 Found!");
  //     init_ok = true;
  //     // mpu_temp = mpu.getTemperatureSensor();
  //     // mpu_temp->printSensorDetails();

  //     mpu_accel = mpu.getAccelerometerSensor();
  //     // mpu_accel->printSensorDetails();

  //     // mpu_gyro = mpu.getGyroSensor();
  //     // mpu_gyro->printSensorDetails();
  //   }
  // }
  // else if (imu_addr == 0x1E)
  // {
  //   imu_select = KX0231025;

  //   imu = new KX0231025Class(Wire, 0x1E);

  //   if (imu->begin(KX0231025_LOWPOWER_MODE, KX0231025_RANGE_8G, KX0231025_DATARATE_100HZ))
  //   {
  //     Serial.println("KX0231025 Found!");
  //     init_ok = true;
  //   }
  //   else
  //   {
  //     init_ok = false;
  //   }
  // }
  // else
  // {
  //   imu_select = no_imu;
  // }

  return init_ok;
  // while(1);
}

float x, y, z;

void getEnyAccBuffer(uint8_t *MemsData)
{
  if (imu_select == mpu6050)
  {
    //  /* Get a new normalized sensor event */
    sensors_event_t accel;
    // sensors_event_t gyro;
    // sensors_event_t temp;
    // mpu_temp->getEvent(&temp);
    mpu_accel->getEvent(&accel);
    // mpu_gyro->getEvent(&gyro);

    // sprintf(gbuf,"Ax:%.2f  Ay:%.2f  Az:%.2f   m/s^2", accel.acceleration.x, accel.acceleration.y, accel.acceleration.z);
    // Serial.println(gbuf);

    ax = (int16_t)((accel.acceleration.x + 180.0F) * 100.0F);
    ay = (int16_t)((accel.acceleration.y + 180.0F) * 100.0F);
    az = (int16_t)((accel.acceleration.z + 180.0F) * 100.0F);

    MemsData[0] = (uint8_t)((ax & 0xff00) >> 8);
    MemsData[1] = (uint8_t)(ax & 0x00ff);

    MemsData[2] = (uint8_t)(ay >> 8);
    MemsData[3] = (uint8_t)(ay & 0xff);

    MemsData[4] = (uint8_t)(az >> 8);
    MemsData[5] = (uint8_t)(az & 0xff);
  }
  else if (imu_select == KX0231025)
  {

    imu->readAcceleration(x, y, z);

    ax = (int16_t)(((x * 9.807) + 180.0F) * 100.0F);
    ay = (int16_t)(((y * 9.807) + 180.0F) * 100.0F);
    az = (int16_t)(((z * 9.807) + 180.0F) * 100.0F);

    MemsData[0] = (uint8_t)((ax & 0xff00) >> 8);
    MemsData[1] = (uint8_t)(ax & 0x00ff);

    MemsData[2] = (uint8_t)(ay >> 8);
    MemsData[3] = (uint8_t)(ay & 0xff);

    MemsData[4] = (uint8_t)(az >> 8);
    MemsData[5] = (uint8_t)(az & 0xff);
  }
  else
  {
    ax = (int16_t)((0.00 + 180.0F) * 100.0F);
    ay = (int16_t)((0.00 + 180.0F) * 100.0F);
    az = (int16_t)((0.00 + 180.0F) * 100.0F);

    MemsData[0] = (uint8_t)((ax & 0xff00) >> 8);
    MemsData[1] = (uint8_t)(ax & 0x00ff);

    MemsData[2] = (uint8_t)(ay >> 8);
    MemsData[3] = (uint8_t)(ay & 0xff);

    MemsData[4] = (uint8_t)(az >> 8);
    MemsData[5] = (uint8_t)(az & 0xff);
  }
}

void test_MPU6050()
{
  //     accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  //     // these methods (and a few others) are also available
  //     //accelgyro.getAcceleration(&ax, &ay, &az);
  //     //accelgyro.getRotation(&gx, &gy, &gz);

  // #ifdef OUTPUT_READABLE_ACCELGYRO
  //     // // display tab-separated accel/gyro x/y/z values
  //     // Serial.print("a/g:\t");
  //     // Serial.print(ax); Serial.print("\t");
  //     // Serial.print(ay); Serial.print("\t");
  //     // Serial.print(az); Serial.print("\t");
  //     // Serial.print(gx); Serial.print("\t");
  //     // Serial.print(gy); Serial.print("\t");
  //     // Serial.println(gz);
  // #endif

  // #ifdef OUTPUT_BINARY_ACCELGYRO
  //     Serial.write((uint8_t)(ax >> 8));
  //     Serial.write((uint8_t)(ax & 0xFF));
  //     Serial.write((uint8_t)(ay >> 8));
  //     Serial.write((uint8_t)(ay & 0xFF));
  //     Serial.write((uint8_t)(az >> 8));
  //     Serial.write((uint8_t)(az & 0xFF));
  //     Serial.write((uint8_t)(gx >> 8));
  //     Serial.write((uint8_t)(gx & 0xFF));
  //     Serial.write((uint8_t)(gy >> 8));
  //     Serial.write((uint8_t)(gy & 0xFF));
  //     Serial.write((uint8_t)(gz >> 8));
  //     Serial.write((uint8_t)(gz & 0xFF));
  // #endif
}