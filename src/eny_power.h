#ifndef ENY_POWER_H
#define ENY_POWER_H

#include <main.h>

#define LED_RED 2
#define LED_GRN 4
#define LED_BLU 5

#define PGOOD 35
#define CHG_STAT 34
#define ADC_Batt 39
#define PWR_MAIN 32
#define Button 13

#define LED_ON 0
#define LED_OFF 1

#define ON 1
#define OFF 0

#define max_batt 4.19f
#define min_batt 3.7f

#define max_batper 100
#define min_battper 5

class eny_Hardware
{
    bool statT = false;
    bool Ready = false;
    unsigned long PushedMill;
    unsigned long currentMill;
    unsigned long turnOnDelay = 5000;
    float Vin;
    float newVal;
    float preBatt;
    uint16_t preCntBatt = 0;
    float batt_v;
    float batt_per;
    bool button;

    uint32_t onBattTime;
    uint32_t onChgTime;

public:
    void mainPowerCrl(bool pwr);
    void init_hardware();
    void ledRGB(bool r, bool g, bool b);

    bool readButton();
    bool get_ChgStatus();
    bool get_Pgood();
    void pwrMNG();
    void enyHWrun();
    float getBattVolt();
    float getBattPerC();
    uint32_t getOnBattTime();
    void saveStat(uint stat);
    void seve_onbatt_time(uint32_t val);
    uint32_t read_onbatt_time(void);

private:
    float smooth(float data, float filterVal, float smoothedVal);
    float read_vBatt();
    int Read_Batt_percent();
};

#if !defined(NO_GLOBAL_INSTANCES) && !defined(NO_GLOBAL_ENY_POWER)
extern eny_Hardware enyHW;
#endif

#endif
