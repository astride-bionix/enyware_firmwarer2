//*********************************************************************************************
//*********************************************************************************************
// Astride Bionix Co,.Ltd
// Project           : Enyware
// Chip number       : ESP32-PICO-D4
// Communication     : BLE
// Hardware version  : Enyware Rev.F - Rev.G
// Developer         : Mr.Meta Boonma
// Email             : metasound00@gmail.com
//*********************************************************************************************
//*********************************************************************************************

#include <main.h>
#include <utility.h>
#include <eny_BLE.h>
#include <eny_MPU6050.h>
#include <eny_power.h>
// #include <rom/rtc.h>
#include "soc/rtc_wdt.h"

#ifdef __cplusplus
extern "C"
{
#endif

  uint8_t temprature_sens_read();

#ifdef __cplusplus
}
#endif

#define F2DW(floatNum) *((unsigned long *)&floatNum)
#define jsonBuffSize TXbufferSize

BLE ble;

TickTask TXdataTick(500);
TickTask onConnectTick(1000);
TickTask recTableTick(500);

TickTask ledTick(1000);

TickTask cntWDTTick(1000UL);

TickTask cnt_OTATick(1000UL);
TickTask OTA_timeoutTick(1200000UL); // 20min

LEDBlink BLE_blue(5);
LEDBlink BLE_green(4);
LEDBlink BLE_red(2);

int sendCount = 0;
char sbuf[TXbufferSize];
bool isRec;

uint8_t enybuff[2048];
uint8_t rxData[2048];
uint8_t cmdbuff[100];
uint8_t rxcmd[100];

uint8_t tableBuffer[1024][6];
uint8_t rxTable[1024][6];

uint8_t battPercent = 100;
bool chgStat = true;
uint8_t testbyteVer[8] = {0x68, 0x07, 0x64, 0x09, 0x78, 0x42, 0xB0, 0x07};

unsigned long currentTick, wdtBLEtick;
unsigned long wdtBLEdelay = 900000; // 15min
// unsigned long wdtBLEdelay = 60000; // 1min

unsigned long currentRecTick, RecTick;
unsigned long RecTickdelay = 500; // 500ms

int cnt_tick = 0;
int cnt_ota_tick = 0;

int set_period;
int set_row = 40;
bool cmd_stat = false;

struct EnyTable_Acc
{
  uint8_t totalRow;
  uint8_t id_txBLE;
  uint16_t frameSize;
  uint16_t dataSize_nfreme;
  uint16_t tableSize;
  uint16_t startTableIndex;
  uint16_t endTableIndex;

  float Period;
  float freq;
  uint32_t recTimeMS;
  uint32_t id_previous;
  uint32_t id_lastUpdate;

  float vbatt;
  uint8_t percent_batt;
  bool pgood;
  bool charge;
  bool button;
} Enyware;

void init_WDT()
{
  //------------- WDT ---------------
  rtc_wdt_protect_off(); // Disable RTC WDT write protection
  rtc_wdt_set_stage(RTC_WDT_STAGE1, RTC_WDT_STAGE_ACTION_RESET_SYSTEM);
  rtc_wdt_set_time(RTC_WDT_STAGE1, 10000); //(Ms)    300000 5 Minit , 10800000 3 hours
  rtc_wdt_enable();
}

void HWstatus()
{
  Enyware.vbatt = enyHW.getBattVolt();
  Enyware.percent_batt = enyHW.getBattPerC();
  Enyware.pgood = enyHW.get_Pgood();
  Enyware.charge = enyHW.get_ChgStatus();
  Enyware.button = enyHW.readButton();
}

// void Eny_recTable()
// {
//   int countRec = 0;
//   int tableSize = 0;
//   uint8_t XYZ_6byte[6];

//   while (true)
//   {
//     if (recTableTick.Update())
//     {
//       break;
//     }
//     else
//     {
//       for (int i = 0; i < 6; i++)
//       {
//         getEnyAccBuffer(XYZ_6byte);
//         tableBuffer[countRec][i] = XYZ_6byte[i];
//         tableSize++;
//       }
//       countRec++;
//     }
//   }
//   Enyware.tableSize = tableSize;
//   Enyware.totalRow = countRec;
//   Enyware.Period = (float)recTableTick.getTick() / (float)countRec;
//   Enyware.freq = (float)(1 / Enyware.Period) * 1000.0f;
//   Enyware.recTimeMS = recTableTick.getTick();
//   Enyware.id_lastUpdate++;
// }

void Eny_recTable()
{
  int countRec = 0;
  int tableSize = 0;
  uint8_t XYZ_6byte[6];

  while (true)
  {
    currentRecTick = millis();
    if ((unsigned long)(currentRecTick - RecTick) >= RecTickdelay)
    {

      // sprintf(sbuf, "TX time : %lu", RecTickdelay);
      // Serial.println(sbuf);

      RecTick = currentRecTick;
      break;
    }
    else
    {

      if (countRec < set_row)
      {

        for (int i = 0; i < 6; i++)
        {
          getEnyAccBuffer(XYZ_6byte);
          tableBuffer[countRec][i] = XYZ_6byte[i];
          tableSize++;
        }
        countRec++;
      }
    }
  }
  Enyware.tableSize = tableSize;
  Enyware.totalRow = countRec;
  Enyware.Period = (float)RecTickdelay / (float)countRec;
  Enyware.freq = (float)(1 / Enyware.Period) * 1000.0f;
  Enyware.recTimeMS = RecTickdelay;
  Enyware.id_lastUpdate++;
}

void print_EnywareTXdata()
{

  sprintf(sbuf, "-> [%d]Buffer : ", Enyware.frameSize);
  Serial.print(sbuf);
  for (int i = 0; i < Enyware.frameSize; i++)
  {
    sprintf(sbuf, "%02X ", enybuff[i]);
    Serial.print(sbuf);
  }
  Serial.println();
}

void Eny_dataByteArr()
{
  uint8_t chksum = 0;
  uint16_t index = 0;
  uint8_t indexBH = 1;
  uint8_t indexBL = 2;

  //[stx][BH][BL][cmd][fwver][TXid][batt][freq BH][freq BL][totalRow][xbh][xbl][ybh][ybl][zbh][zbl]..n...[ybl][zbh][zbl][chksum][eot]

  enybuff[index++] = stx;                //[0]Start frame
  enybuff[index++] = 0;                  //[1]BH
  enybuff[index++] = 0;                  //[2]BL
  enybuff[index++] = CMDtableTX;         //[3]CMD = 0xF1 = Hardware status
  enybuff[index++] = fwver;              //[4]firmware version
  enybuff[index++] = Enyware.id_txBLE++; //[5]ble tx counter ID

  uint8_t PWRdata = ((uint8_t)Enyware.percent_batt & 0x7F);
  enybuff[index++] = PWRdata; //[6] % batterry and charge stat 0-100

  uint16_t freqX100 = Enyware.freq * 100;          // freq convert [7][8]
  enybuff[index++] = (uint8_t)(freqX100 >> 8);     //[7]freq byte high
  enybuff[index++] = (uint8_t)(freqX100 & 0x00FF); //[8]freq byte low
  enybuff[index++] = Enyware.totalRow;             //[9] Number of row table

  Enyware.startTableIndex = index;

  for (int x = 0; x < Enyware.totalRow; x++)
  {
    for (int i = 0; i < 6; i++)
    {
      enybuff[index++] = tableBuffer[x][i];
    }
  }

  Enyware.endTableIndex = index - 1;
  Enyware.dataSize_nfreme = index - 3;

  // Serial.println();
  for (int i = 3; i < index; i++)
  {
    chksum ^= enybuff[i];
  }
  // Serial.println();

  enybuff[indexBH] = (uint8_t)(((Enyware.dataSize_nfreme) >> 8));
  enybuff[indexBL] = (uint8_t)((Enyware.dataSize_nfreme) & 0x00FF);
  enybuff[index++] = chksum;
  enybuff[index++] = eot;

  Enyware.frameSize = index;
}

bool verifyTXdata(uint8_t *inDataBuff, uint8_t *outDataBuff)
{
  uint8_t chksum_cal = 0;
  bool isVerify = false;
  if (inDataBuff[0] == stx)
  {

    uint16_t sizeOfData = (uint16_t)((inDataBuff[1] * 256) + inDataBuff[2]);
    uint16_t startverify = 3;
    uint16_t endverify = sizeOfData + 3;
    uint16_t out = 0;

    for (int i = startverify; i < endverify; i++)
    {
      chksum_cal ^= inDataBuff[i];
      outDataBuff[out++] = inDataBuff[i];
    }

    uint8_t chksum_read = inDataBuff[endverify];
    uint8_t eot_read = inDataBuff[endverify + 1];

    if (eot_read == 0x04)
    {
      if (chksum_read == chksum_cal)
      {
        Serial.println("--- [verify] -> OK");
        isVerify = true;
      }
      else
        Serial.println("--- [verify] -> Checksum Error");
    }
    else
      Serial.println("--- [verify] -> EOT error");
  }
  return isVerify;
}

//[cmd][fwver][TXid][batt][freq BH][freq BL][totalRow][xbh][xbl][ybh][ybl][zbh][zbl]..n...[ybl][zbh][zbl]

float EnyMotionTable[1024][3];

void Eny_convertData(uint8_t *enybuff)
{
  uint16_t index = 0;

  if (verifyTXdata(enybuff, rxData))
  { // return true if checksum match
    uint8_t cmd = rxData[index++];
    uint8_t fwv = rxData[index++];
    uint8_t idRx = rxData[index++];
    uint8_t batt = rxData[index++];
    uint8_t fqH = rxData[index++];
    uint8_t fqL = rxData[index++];
    float freqHz = ((float)(uint16_t)((fqH << 8) | fqL) / 100);

    uint8_t totalRow = rxData[index++];

    for (uint8_t i = 0; i < totalRow; i++)
    {
      for (uint8_t x = 0; x < 3; x++)
      {
        uint8_t bh = rxData[index++];
        uint8_t bl = rxData[index++];
        uint16_t val = (int)((bh << 8) | bl);
        EnyMotionTable[i][x] = ((float)(val / 100.0F) - 180.0F);
      }
    }
    sprintf(sbuf, "[BLE] -> [DATA] cmd: %02X, firmware: %003d, id: %d, batt: %d, freqHz: %.2f, row: %d",
            cmd, fwv, idRx, batt, freqHz, totalRow);
    Serial.println(sbuf);

    sprintf(sbuf, "[BLE] -> [ACC] Ex row1 : %.2f, %.2f, %.2f", EnyMotionTable[0][0], EnyMotionTable[0][1], EnyMotionTable[0][2]);
    Serial.println(sbuf);
  }
}

// cmd = CMDseting :  [stx][BH][BL][cmd][tableRow][timHI][timLO][chksum][eot]
// ex : 02 00 04 A4 14 01 F4 45 04

void command_rec(uint8_t *buff, int len)
{
  uint16_t index = 0;
  Serial.print("[BLE] -> command rx:");
  for (int i = 0; i < len; i++)
  {
    sprintf(sbuf, "0x%02X ", buff[i]);
    Serial.print(sbuf);
  }
  Serial.println("\n**************");

  if (verifyTXdata(buff, rxcmd))
  { // return true if checksum match
    uint8_t cmd = rxcmd[index++];
    if (cmd == CMDseting)
    {
      uint8_t Row = rxcmd[index++];
      uint8_t period_h = rxcmd[index++];
      uint8_t period_l = rxcmd[index++];

      set_period = ((int)((period_h << 8) | period_l));
      set_row = (int)Row;

      RecTickdelay = set_period;
      cmd_stat = true;

      sprintf(sbuf, "cmd: %02X, Row: %003d, period: %d",
              cmd, set_row, set_period);
      Serial.println(sbuf);
    }
  }
}

void cmdConvertData(uint8_t *cmdbuf)
{
  uint16_t index = 0;

  if (verifyTXdata(cmdbuf, rxcmd))
  { // return true if checksum match
    uint8_t cmd = rxcmd[index++];
    uint8_t Row = rxcmd[index++];
    uint8_t period_h = rxcmd[index++];
    uint8_t period_l = rxcmd[index++];

    int period = ((int)((period_h << 8) | period_l));

    sprintf(sbuf, "cmd: %02X, Row: %003d, period: %d",
            cmd, Row, period);
    Serial.println(sbuf);
  }
}

void testCMD()
{

  uint8_t index = 0;
  uint8_t chksum = 0;
  uint8_t indexBH = 1;
  uint8_t indexBL = 2;

  cmdbuff[index++] = stx;       //[0]Start frame
  cmdbuff[index++] = 0;         //[1]BH
  cmdbuff[index++] = 0;         //[2]BL
  cmdbuff[index++] = CMDseting; //[3]CMD = 0xA4

  uint8_t Row = 20;
  cmdbuff[index++] = Row; //[4]number of Row

  uint16_t period = 500;                         // period convert [5][6]
  cmdbuff[index++] = (uint8_t)(period >> 8);     //[5]period byte high
  cmdbuff[index++] = (uint8_t)(period & 0x00FF); //[6]period byte low

  uint8_t cmdIndex = index - 1;
  uint8_t cmdnf = index - 3;

  // Serial.println();
  for (int i = 3; i < index; i++)
  {
    chksum ^= cmdbuff[i];
  }
  // Serial.println();

  cmdbuff[indexBH] = (uint8_t)(((cmdnf) >> 8));
  cmdbuff[indexBL] = (uint8_t)((cmdnf)&0x00FF);
  cmdbuff[index++] = chksum;
  cmdbuff[index++] = eot;

  Serial.print("--> CMD test : ");
  for (int i = 0; i < index; i++)
  {
    sprintf(sbuf, "0x%02X, ", cmdbuff[i]);
    Serial.print(sbuf);
  }
  Serial.println("\n");

  cmdConvertData(cmdbuff);
}

//================================= Main process ===================================

void setup()
{

  Serial.begin(115200);
  Wire.begin();
  Wire.setClock(50000);

  enyHW.init_hardware();
  getChipID();

  char DevName[40];
  Serial.println("\n");
  Serial.println("=== init Enyware ===");
  sprintf(DevName, "%s_%s", DeviceName, getChipID());
  Serial.println(DevName);

  ble.begin(DevName);

  scanI2C_device();
  delay(100);
  if (init_enyware_MPU6050() == true)
  {
    Serial.println("\n==== Enyware Hardware OK ====");
    Serial.println("==== Start Enyware-pico32 ! ====\n");
  }
  else
  {
    Serial.println("==== IMU sensor error ! ====");
    Serial.println("==== Please check IMU IC! or I2C interface ====");
    while (1)
      ;
  }

  init_WDT();

  // uint8_t *HWid = getChipID();

  sprintf(sbuf, "Firmware version : %s, build : %s\n", FWversion, Build_DT);
  Serial.println(sbuf);
  sprintf(sbuf, "Chip model: %s, Rev %d", ESP.getChipModel(), ESP.getChipRevision());
  Serial.println(sbuf);
  // sprintf(sbuf,"Chip ID: %02X:%02X:%02X:%02X:%02X:%02X",HWid[0], HWid[1], HWid[2], HWid[3], HWid[4], HWid[5]);
  Serial.print("Chip ID : ");
  Serial.println(getChipID_string());

  Serial.println("=================================\n\n");

  enyHW.ledRGB(0, 0, 0);

  // //---------------Task-------------
  xTaskCreatePinnedToCore(
      Main_task,   /* Task function. */
      "Main_task", /* name of task. */
      5000,        /* Stack size of task */
      NULL,        /* parameter of the task */
      3,           /* priority of the task */
      NULL,        /* Task handle to keep track of created task */
      1);

  xTaskCreatePinnedToCore(
      MPU_task,   /* Task function. */
      "MPU_task", /* name of task. */
      5000,       /* Stack size of task */
      NULL,       /* parameter of the task */
      3,          /* priority of the task */
      NULL,       /* Task handle to keep track of created task */
      1);
}

void loop()
{
  // do not use
}

bool onConnectBLE = false;

void Main_task(void *pvParameter)
{
  Serial.println("=========== Start Main task ===========");

  while (true)
  {
    rtc_wdt_feed();
    enyHW.enyHWrun();
    HWstatus();

    currentTick = millis();
    // currentRecTick = millis();

    BLE_blue.ledblinkRun();
    BLE_green.ledblinkRun();
    BLE_red.ledblinkRun();

    if (OTAstart == true)
    {
      // LED White blink (on 200ms, off 1800ms)
      BLE_red.ledUpdate(1800, 200);
      BLE_blue.ledUpdate(1800, 200);
      BLE_green.ledUpdate(1800, 200);

      if (OTA_timeoutTick.Update())
      { // TimeOut 15 Min : restart ESP
        printlnDebug("[OTA] -> Time out! (20Min) : restart Enyware");
        delay(2000);
        esp_restart();
      }
      if (cnt_OTATick.Update())
      {
        cnt_ota_tick++;
        int h = (cnt_ota_tick / 3600);
        int m = (cnt_ota_tick - (3600 * h)) / 60;
        int s = (cnt_ota_tick - (3600 * h) - (m * 60));

        // float timeOTA = ((float)cnt_ota_tick/60.0f);
        sprintf(sbuf, "[OTA] -> Wait recieve firmware complete : [%02d:%02d]", m, s);
        printlnDebug(sbuf);
      }
    }
    else
    {
      if (ble.run_enywareBLE_connected())
      {
        onConnectBLE = true;

        if (Enyware.percent_batt <= 20)
        {
          if ((enyHW.get_Pgood() == 0) && (enyHW.get_ChgStatus() == 0))
          {
            enyHW.ledRGB(1, 1, 0);
          }
          else if ((enyHW.get_Pgood() == 0) && (enyHW.get_ChgStatus() == 1))
          {
            enyHW.ledRGB(0, 1, 0);
          }
          else
          {
            BLE_red.ledUpdate(2000, 100);
            BLE_blue.ledStop();
            BLE_green.ledStop();
          }
        }
        else
        {
          // enyHW.ledRGB(0,0,0);
          if ((enyHW.get_Pgood() == 0) && (enyHW.get_ChgStatus() == 0))
          {
            enyHW.ledRGB(1, 1, 0);
          }
          else if ((enyHW.get_Pgood() == 0) && (enyHW.get_ChgStatus() == 1))
          {
            enyHW.ledRGB(0, 1, 0);
          }
          else
          {
            BLE_green.ledUpdate(2000, 100);
            BLE_blue.ledStop();
            if (Enyware.charge == 1)
            {
              BLE_red.ledStop();
            }
          }
          // ble.Send_Databuffer_BLE(enybuff, Enyware.frameSize);
          // ble.updateBattery(Enyware.percent_batt);
        }
      }
      else
      {

        onConnectBLE = false;
        if ((enyHW.get_Pgood() == 0) && (enyHW.get_ChgStatus() == 0))
        {
          enyHW.ledRGB(1, 1, 0);
        }
        else if ((enyHW.get_Pgood() == 0) && (enyHW.get_ChgStatus() == 1))
        {
          enyHW.ledRGB(0, 1, 0);
        }
        else
        {
          if (Enyware.percent_batt <= 20)
          {
            BLE_red.ledUpdate(100, 100);
            BLE_blue.ledStop();
            BLE_green.ledStop();
          }
          else
          {
            // enyHW.ledRGB(1,0,0);

            BLE_blue.ledUpdate(100, 100);
            BLE_green.ledStop();
            if (Enyware.charge == 1)
            {
              BLE_red.ledStop();
            }
            // BLE_blue.ledblink(100);
          }
        }
      }

      if (ledTick.Update())
      {
        sprintf(sbuf, "[HW] -> Vbatt : %.2f, percent : %d, pgood: %d, charge : %d, button : %d, onbatt_time : %d, eeprom_onbatt : %d",
                Enyware.vbatt, Enyware.percent_batt, Enyware.pgood, Enyware.charge, Enyware.button, enyHW.getOnBattTime(), enyHW.read_onbatt_time());
        Serial.println(sbuf);

        // testCMD();
      }

      if (onConnectBLE)
      {

        if (onConnectTick.Update())
        {
        }

        if (Enyware.id_previous < Enyware.id_lastUpdate)
        {

          Eny_dataByteArr();

          Serial.println("--");
          sprintf(sbuf, "[BLE] -> [TB] idRec: %u, row:%d, Recording time: %u ms, period: %.2f ms, frq: %.3f Hz",
                  Enyware.id_lastUpdate, Enyware.totalRow, Enyware.recTimeMS, Enyware.Period, Enyware.freq);
          Serial.println(sbuf);
          sprintf(sbuf, "[BLE] -> [TX] Size [%d] byte , size nfame:[%d], table size :%d, start table@:%d, end table@:%d",
                  Enyware.frameSize, Enyware.dataSize_nfreme, Enyware.tableSize, Enyware.startTableIndex, Enyware.endTableIndex);
          Serial.println(sbuf);
          // Serial.println("---------------------------------");

          ble.Send_Databuffer_BLE(enybuff, Enyware.frameSize);
          ble.updateBattery(Enyware.percent_batt);
          ble.updateBattCharging(Enyware.charge);
          ble.updateBatt_chg_to_sensorUUID();
          // ble.updateBattCharging(0x01);

          // print_EnywareTXdata();

          Eny_convertData(enybuff);

          Enyware.id_previous = Enyware.id_lastUpdate;
        }

        wdtBLEtick = currentTick;
        cnt_tick = 0;
      }
      else
      {

        if (enyHW.get_Pgood() != 0)
        {

          if ((unsigned long)(currentTick - wdtBLEtick) >= wdtBLEdelay)
          {
            int timmin = (int)((wdtBLEdelay / 60.0f) / 1000.0f);
            sprintf(sbuf, "[PWR] -> Shutdown because there was no connection at the specified time. (%d min)", timmin);
            printlnDebug(sbuf);

            delay(1000);
            enyHW.mainPowerCrl(OFF);
            delay(1000);
          }

          if (cntWDTTick.Update())
          {
            cnt_tick++;
            int h = (cnt_tick / 3600);
            int m = (cnt_tick - (3600 * h)) / 60;
            int s = (cnt_tick - (3600 * h) - (m * 60));

            sprintf(sbuf, "[PWR] -> Wait BLE on batt connect : [%02d:%02d]\n", m, s);
            printlnDebug(sbuf);
          }
        }
      }
      vTaskDelay(1);
    }
  }
}

void MPU_task(void *pvParameter)
{
  Serial.println("=========== Strat MPU task ===========");
  while (true)
  {
    if (OTAstart == false)
    {
      Eny_recTable();
    }
  }
}
